<?php

namespace App\Repository;

use App\Model\User as UserModel;

class User
{
    /**
     * @param int $perPage
     * @param string|null $search
     * @return mixed
     */
    public function list(int $perPage, string $search = null)
    {
        if(!empty($search)) {
            return UserModel::where('name', 'RLIKE', $search)->paginate($perPage);
        }

        return UserModel::paginate($perPage);
    }

    /**
     * @param int $id
     * @return UserModel
     */
    public function get(int $id)
    {
        return UserModel::find($id);
    }

    /**
     * @param array $data
     * @return UserModel
     */
    public function store(array $data)
    {
        return UserModel::create($data);
    }

    /**
     * @param UserModel $User
     * @param $data
     * @return UserModel
     */
    public function update(UserModel $User, $data)
    {
        $User->fill($data);
        $User->save();
        return $User;
    }

    /**
     * @param UserModel $User
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(UserModel $User)
    {
        return $User->delete();
    }

    /**
     * @param string $column
     * @param string $value
     * @return mixed
     */
    private function getByColumn(string $column, string $value)
    {
        return UserModel::where($column, $value);
    }

    /**
     * @param string $column
     * @param string $value
     * @return UserModel
     */
    public function getOneByColumn(string $column, string $value)
    {
        return $this->getByColumn($column, $value)->first();
    }

    /**
     * @param string $column
     * @param string $value
     * @return mixed
     */
    public function getAllByColumn(string $column, string $value)
    {
        return $this->getByColumn($column, $value)->get();
    }
}
