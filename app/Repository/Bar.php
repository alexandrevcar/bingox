<?php

namespace App\Repository;

use App\Model\Bar as BarModel;

class Bar
{
    /**
     * @param int $perPage
     * @return mixed
     */
    public function list(int $perPage)
    {
        return BarModel::with('user')->with('room')->paginate($perPage);
    }

    /**
     * @param int $id
     * @return BarModel
     */
    public function get(int $id)
    {
        return BarModel::with('user')->with('room')->find($id);
    }

    /**
     * @param array $data
     * @return BarModel
     */
    public function store(array $data)
    {
        return BarModel::create($data);
    }

    /**
     * @param BarModel $bar
     * @param $data
     * @return BarModel
     */
    public function update(BarModel $bar, $data)
    {
        $bar->fill($data);
        $bar->save();
        return $bar;
    }

    /**
     * @param BarModel $bar
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(BarModel $bar)
    {
        return $bar->delete();
    }
}
