<?php

namespace App\Repository;

use App\Model\Room as RoomModel;

class Room
{
    /**
     * @param int $perPage
     * @return mixed
     */
    public function list(int $perPage)
    {
        return RoomModel::with('user')->paginate($perPage);
    }

    /**
     * @param int $id
     * @return RoomModel
     */
    public function get(int $id)
    {
        return RoomModel::with('user')->find($id);
    }

    /**
     * @param array $data
     * @return RoomModel
     */
    public function store(array $data)
    {
        return RoomModel::create($data);
    }

    /**
     * @param RoomModel $Room
     * @param $data
     * @return RoomModel
     */
    public function update(RoomModel $Room, $data)
    {
        $Room->fill($data);
        $Room->save();
        return $Room;
    }

    /**
     * @param RoomModel $Room
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(RoomModel $Room)
    {
        return $Room->delete();
    }
}
