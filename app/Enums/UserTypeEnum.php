<?php

namespace App\Enums;


use MyCLabs\Enum\Enum;

class UserTypeEnum extends Enum
{
    const ADMIN = 'admin';
    const PARTNER = 'partner';
    const SELLER = 'seller';
    const ESTABLISHMENT = 'establishment';
}
