<?php

namespace App\Enums;


use MyCLabs\Enum\Enum;

class StatusEnum extends Enum
{
    const INACTIVE = 0;
    const ACTIVE = 1;
}
