<?php

namespace App\Enums;


use MyCLabs\Enum\Enum;

class ColumnsNumberTypeEnum extends Enum
{
    const FIRST_COLUMN = 1;
    const SECOND_COLUMN = 2;
    const THIRD_COLUMN = 3;
}
