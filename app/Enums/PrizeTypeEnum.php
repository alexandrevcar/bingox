<?php

namespace App\Enums;


use MyCLabs\Enum\Enum;

class PrizeTypeEnum extends Enum
{
    const KUADRA = 'kuadra';
    const KINA = 'kina';
    const KENO = 'keno';
}
