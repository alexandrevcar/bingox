<?php

namespace App\Model\Card;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $table = 'cards_numbers';
    
    protected $fillable = ['card_id', 'number', 'column'];

    function card() {
        return $this->belongsTo('App\Model\Card');
    }
}
