<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    protected $fillable = ['type', 'name', 'email', 'password', 'status'];

    protected $visible = ['id', 'name', 'email', 'password', 'type', 'status', 'created_at', 'updated_at'];

    public function bars()
    {
        return $this->hasMany('App\Model\Bar');
    }

    public function raffles()
    {
        return $this->hasMany('App\Model\Raffle');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
