<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bar extends Model
{
    protected $fillable = ['user_id', 'room_id', 'name', 'postcode', 'address', 'number', 'neighborhood', 'city', 'state', 'status'];

    function user() {
        return $this->belongsTo('App\Model\User');
    }

    function room() {
        return $this->belongsTo('App\Model\Room');
    }

    public function cards()
    {
        return $this->hasMany('App\Model\Card');
    }
}
