<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = ['bar_id', 'raffle_id', 'status'];

    function bar() {
        return $this->belongsTo('App\Model\Bar');
    }

    function raffle() {
        return $this->belongsTo('App\Model\Raffle');
    }

    public function numbers()
    {
        return $this->hasMany('App\Model\Card\Number');
    }

    public function prizes()
    {
        return $this->hasMany('App\Model\Raffle\Prize');
    }
}
