<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Raffle extends Model
{
    protected $fillable = ['user_id', 'date', 'kuadra', 'kina', 'keno', 'status'];

    function user() {
        return $this->belongsTo('App\Model\User');
    }

    public function cards()
    {
        return $this->hasMany('App\Model\Card');
    }

    public function prizes()
    {
        return $this->hasMany('App\Model\Raffle\Prize');
    }

    public function numbers()
    {
        return $this->hasMany('App\Model\Raffle\Number');
    }
}
