<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['user_id', 'name', 'status'];

    function user() {
        return $this->belongsTo('App\Model\User');
    }

    public function bars()
    {
        return $this->hasMany('App\Model\Bar');
    }
}
