<?php

namespace App\Model\Raffle;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    protected $table = 'raffles_prizes';

    protected $fillable = ['card_id', 'raffle_id', 'prize', 'type_prize'];

    function card() {
        return $this->belongsTo('App\Model\Card');
    }

    function raffle() {
        return $this->belongsTo('App\Model\Raffle');
    }
}
