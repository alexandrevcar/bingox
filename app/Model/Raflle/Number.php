<?php

namespace App\Model\Raffle;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $table = 'raffles_numbers';

    protected $fillable = ['raffle_id', 'number'];

    function raffle() {
        return $this->belongsTo('App\Model\Raffle');
    }
}
