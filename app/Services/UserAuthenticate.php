<?php

namespace App\Services;

use App\Enums\StatusEnum;
use App\Repository\User as UserRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserAuthenticate
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function authUser($email, $password)
    {
        // Get user by email
        $user = $this->userRepository->getOneByColumn('email', $email);

        if(empty($user)) {
            throw new \Exception('Invalid credentials', Response::HTTP_UNAUTHORIZED);
        }

        // Validate Password and User inactive
        if (!Hash::check($password, $user->password) || $user->status == StatusEnum::INACTIVE) {
            throw new \Exception('Invalid credentials', Response::HTTP_UNAUTHORIZED);
        }

        // Generate Token
        $token = JWTAuth::fromUser($user);

        // Get expiration time
        $objectToken = JWTAuth::setToken($token);
        $expiration = JWTAuth::decode($objectToken->getToken())->get('exp');

        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $expiration,
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'type' => $user->type
            ]
        ];
    }
}