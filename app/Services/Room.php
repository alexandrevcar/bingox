<?php

namespace App\Services;

use App\Repository\Room as RoomRepository;
use Illuminate\Http\Response;

class Room
{
    /**
     * @var RoomRepository
     */
    protected $roomRepository;

    public function __construct(RoomRepository $RoomRepository)
    {
        $this->roomRepository = $RoomRepository;
    }

    /**
     * @param int $perPage
     * @return mixed
     */
    public function list(int $perPage)
    {
        $rooms = $this->roomRepository->list($perPage);
        foreach ($rooms as $room) {
            $room->statusLabel = ($room->status ? 'Ativo' : 'Inativo');
        }
        return $rooms;
    }

    /**
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function get(int $id)
    {
        $room = $this->roomRepository->get($id);

        if(empty($room)) {
            throw new \Exception(
                Response::$statusTexts[Response::HTTP_NOT_FOUND],
                Response::HTTP_NOT_FOUND
            );
        }
        return $room;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function store(array $data)
    {
        return $this->roomRepository->store($data);
    }

    /**
     * @param \App\Model\Room $room
     * @param array $data
     * @return \App\Model\Room
     */
    public function update(\App\Model\Room $room, array $data)
    {
        $room = $this->roomRepository->update($room, $data);
        return $room;
    }

    /**
     * @param \App\Model\Room $room
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(\App\Model\Room $room)
    {
        return $this->roomRepository->destroy($room);
    }

}