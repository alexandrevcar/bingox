<?php

namespace App\Services\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator as ValidatorFacade;

class Validate
{
    /**
     * @param Request $request
     * @return array
     */
    public function valid(Request $request)
    {
        $validator = ValidatorFacade::make($request->toArray(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            throw new \InvalidArgumentException($validator->errors()->first(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $request->toArray();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function validUpdate(Request $request)
    {
        $validator = ValidatorFacade::make($request->toArray(), [
            'email' => 'email'
        ]);

        if ($validator->fails()) {
            throw new \InvalidArgumentException($validator->errors()->first(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $request->toArray();
    }
}