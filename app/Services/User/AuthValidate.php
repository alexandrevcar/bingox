<?php

namespace App\Services\User;

use App\Services\ValidateAbstract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as ValidatorFacade;

class AuthValidate extends ValidateAbstract
{
    /**
     * @param Request $request
     * @return array
     */
    public function valid(Request $request)
    {
        $validator = ValidatorFacade::make($request->toArray(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $this->fails($validator);
        return $request->toArray();
    }
}