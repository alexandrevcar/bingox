<?php

namespace App\Services;

use App\Repository\User as UserRepository;
use Illuminate\Http\Response;

class User
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $perPage
     * @param string|null $search
     * @return mixed
     */
    public function list(int $perPage, string $search = null)
    {
        return $this->userRepository->list($perPage, $search);
    }

    /**
     * @param int $id
     * @return \App\Model\User
     * @throws \Exception
     */
    public function get(int $id)
    {
        $user = $this->userRepository->get($id);

        if(empty($user)) {
            throw new \Exception(
                Response::$statusTexts[Response::HTTP_NOT_FOUND],
                Response::HTTP_NOT_FOUND
            );
        }
        return $user;
    }

    /**
     * @param array $data
     * @return \App\Model\User
     * @throws \Exception
     */
    public function store(array $data)
    {
        return $this->userRepository->store($data);
    }

    /**
     * @param \App\Model\User $user
     * @param array $data
     * @return \App\Model\User
     */
    public function update(\App\Model\User $user, array $data)
    {
        $user = $this->userRepository->update($user, $data);
        return $user;
    }

    /**
     * @param \App\Model\User $user
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(\App\Model\User $user)
    {
        return $this->userRepository->destroy($user);
    }

}