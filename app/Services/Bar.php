<?php

namespace App\Services;

use App\Repository\Bar as BarRepository;
use Illuminate\Http\Response;

class Bar
{
    /**
     * @var BarRepository
     */
    protected $barRepository;

    public function __construct(BarRepository $barRepository)
    {
        $this->barRepository = $barRepository;
    }

    /**
     * @param int $perPage
     * @return mixed
     */
    public function list(int $perPage)
    {
        $bars = $this->barRepository->list($perPage);
        foreach ($bars as $bar) {
            $bar->statusLabel = ($bar->status ? 'Ativo' : 'Inativo');
        }
        return $bars;
    }

    /**
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function get(int $id)
    {
        $bar = $this->barRepository->get($id);

        if(empty($bar)) {
            throw new \Exception(
                Response::$statusTexts[Response::HTTP_NOT_FOUND],
                Response::HTTP_NOT_FOUND
            );
        }
        return $bar;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function store(array $data)
    {
        return $this->barRepository->store($data);
    }

    /**
     * @param \App\Model\Bar $bar
     * @param array $data
     * @return \App\Model\Bar
     */
    public function update(\App\Model\Bar $bar, array $data)
    {
        $bar = $this->barRepository->update($bar, $data);
        return $bar;
    }

    /**
     * @param \App\Model\Bar $bar
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(\App\Model\Bar $bar)
    {
        return $this->barRepository->destroy($bar);
    }

}