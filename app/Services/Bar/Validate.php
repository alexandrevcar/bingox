<?php

namespace App\Services\Bar;

use App\Services\ValidateAbstract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as ValidatorFacade;

class Validate extends ValidateAbstract
{
    /**
     * @param Request $request
     * @return array
     */
    public function valid(Request $request)
    {
        $validator = ValidatorFacade::make($request->toArray(), [
            'user_id' => 'required|integer',
            'room_id' => 'required|integer',
            'name' => 'required|max:150',
            'postcode' => 'required|max:9|min:9',
            'address' => 'required|max:150',
            'number' => 'required|max:10',
            'neighborhood' => 'required|max:150',
            'city' => 'required|max:150',
            'state' => 'required|max:2'
        ]);

        $this->fails($validator);
        return $request->toArray();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function validUpdate(Request $request)
    {
        $validator = ValidatorFacade::make($request->toArray(), [
            'user_id' => 'integer',
            'room_id' => 'integer',
            'name' => 'max:150',
            'postcode' => 'max:9|min:9',
            'address' => 'max:150',
            'number' => 'max:10',
            'neighborhood' => 'max:150',
            'city' => 'max:150',
            'state' => 'max:2'
        ]);

        $this->fails($validator);
        return $request->toArray();
    }
}