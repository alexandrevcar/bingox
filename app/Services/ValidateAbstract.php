<?php

namespace App\Services;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

abstract class ValidateAbstract
{

    /**
     * @param Validator $validator
     * @return bool
     */
    public function fails(Validator $validator)
    {
        if ($validator->fails()) {
            $error = [
                'message' => 'Mandatory fields',
                'fields' => []
            ];
            foreach ($validator->errors()->getMessages() as $fieldName => $message)
            {
                $error['fields'][$fieldName] = Arr::first($message);
            }
            throw new \InvalidArgumentException(json_encode($error), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return true;
    }
}