<?php

namespace App\Services\Room;

use App\Services\ValidateAbstract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as ValidatorFacade;

class Validate extends ValidateAbstract
{
    /**
     * @param Request $request
     * @return array
     */
    public function valid(Request $request)
    {
        $validator = ValidatorFacade::make($request->toArray(), [
            'user_id' => 'required|integer',
            'name' => 'required|max:150'
        ]);

        $this->fails($validator);
        return $request->toArray();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function validUpdate(Request $request)
    {
        $validator = ValidatorFacade::make($request->toArray(), [
            'user_id' => 'integer',
            'name' => 'max:150',
        ]);

        $this->fails($validator);
        return $request->toArray();
    }
}