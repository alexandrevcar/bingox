<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Room as RoomService;
use App\Services\Room\Validate as RoomValidate;

class RoomsController extends Controller
{
    /**
     * @var RoomService
     */
    protected $roomService;

    /**
     * @var RoomValidate
     */
    protected $roomValidate;

    /**
     * RoomsController constructor.
     * @param RoomService $roomService
     * @param RoomValidate $roomValidate
     */
    public function __construct(RoomService $roomService, RoomValidate $roomValidate)
    {
        $this->roomService = $roomService;
        $this->roomValidate = $roomValidate;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $perPage = $request->per_page ?? 50;
            $rooms = $this->roomService->list($perPage);
            return response()->json($rooms, 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $room = $this->roomService->get($id);
            return response()->json($room->toArray(), 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $data = $this->roomValidate->valid($request);
            $room = $this->roomService->store($data);
            return response()->json($room->toArray(), 201);
        } catch (\InvalidArgumentException $e) {
            $error = json_decode($e->getMessage(), true);
            return response()->json([
                'message' => $error['message'],
                'fields' => $error['fields']
            ], $e->getCode());
        }catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this->roomValidate->validUpdate($request);
            $room = $this->roomService->update(
                $this->roomService->get($id),
                $data
            );
            return response()->json($room->toArray(), 201);
        } catch (\InvalidArgumentException $e) {
            $error = json_decode($e->getMessage(), true);
            return response()->json([
                'message' => $error['message'],
                'fields' => $error['fields']
            ], $e->getCode());
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->roomService->destroy($this->roomService->get($id));
            return response()->json([], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }
}
