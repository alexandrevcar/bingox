<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Bar as BarService;
use App\Services\Bar\Validate as BarValidate;

class BarsController extends Controller
{
    /**
     * @var BarService
     */
    protected $barService;

    /**
     * @var BarValidate
     */
    protected $barValidate;

    /**
     * BarsController constructor.
     * @param BarService $barService
     * @param BarValidate $barValidate
     */
    public function __construct(BarService $barService, BarValidate $barValidate)
    {
        $this->barService = $barService;
        $this->barValidate = $barValidate;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $perPage = $request->per_page ?? 50;
            $bars = $this->barService->list($perPage);
            return response()->json($bars, 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $bar = $this->barService->get($id);
            return response()->json($bar->toArray(), 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $data = $this->barValidate->valid($request);
            $bar = $this->barService->store($data);
            return response()->json($bar->toArray(), 201);
        } catch (\InvalidArgumentException $e) {
            $error = json_decode($e->getMessage(), true);
            return response()->json([
                'message' => $error['message'],
                'fields' => $error['fields']
            ], $e->getCode());
        }catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this->barValidate->validUpdate($request);
            $bar = $this->barService->update(
                $this->barService->get($id),
                $data
            );
            return response()->json($bar->toArray(), 201);
        } catch (\InvalidArgumentException $e) {
            $error = json_decode($e->getMessage(), true);
            return response()->json([
                'message' => $error['message'],
                'fields' => $error['fields']
            ], $e->getCode());
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->barService->destroy($this->barService->get($id));
            return response()->json([], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }
}
