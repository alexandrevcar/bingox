<?php

namespace App\Http\Controllers;

use App\Model\Card;
use Illuminate\Http\Request;

class CardsController extends Controller
{
    public function index()
    {
        $card = Card::all();
        return response()->json($card);
    }

    public function show($id)
    {
        $card = Card::find($id);

        if(!$card) {
            return response()->json([
                'message'   => 'Card not found',
            ], 404);
        }

        return response()->json($card);
    }

    public function store(Request $request)
    {
        $card = new Card();
        $card->fill($request->all());
        $card->save();

        return response()->json($card, 201);
    }

    public function update(Request $request, $id)
    {
        $card = Card::find($id);

        if(!$card) {
            return response()->json([
                'message'   => 'Card not found',
            ], 404);
        }

        $card->fill($request->all());
        $card->save();

        return response()->json($card);
    }

    public function destroy($id)
    {
        $card = Card::find($id);

        if(!$card) {
            return response()->json([
                'message'   => 'Card not found',
            ], 404);
        }

        $card->delete();
    }
}
