<?php

namespace App\Http\Controllers;

use App\Model\Raffle;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Http\Request;

class RafflesController extends Controller
{
    use Authenticatable, CanResetPassword;

    public function index()
    {
        $raffle = Raffle::all();
        return response()->json($raffle);
    }

    public function show($id)
    {
        $raffle = Raffle::find($id);

        if(!$raffle) {
            return response()->json([
                'message'   => 'Raffle not found',
            ], 404);
        }

        return response()->json($raffle);
    }

    public function store(Request $request)
    {
        $raffle = new Raffle();
        $raffle->fill($request->all());
        $raffle->save();

        return response()->json($raffle, 201);
    }

    public function update(Request $request, $id)
    {
        $raffle = Raffle::find($id);

        if(!$raffle) {
            return response()->json([
                'message'   => 'Raffle not found',
            ], 404);
        }

        $raffle->fill($request->all());
        $raffle->save();

        return response()->json($raffle);
    }

    public function destroy($id)
    {
        $raffle = Raffle::find($id);

        if(!$raffle) {
            return response()->json([
                'message'   => 'Raffle not found',
            ], 404);
        }

        $raffle->delete();
    }
}
