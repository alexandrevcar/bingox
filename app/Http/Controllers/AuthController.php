<?php

namespace App\Http\Controllers;

use App\Services\User\AuthValidate;
use Illuminate\Http\Request;
use App\Services\UserAuthenticate as UserAuthenticateService;

class AuthController extends Controller
{
    /**
     * @var UserAuthenticateService
     */
    protected $userAuthService;

    /**
     * @var AuthValidate
     */
    protected $authValidate;

    /**
     * AuthController constructor.
     * @param UserAuthenticateService $userService
     * @param AuthValidate $authValidate
     */
    public function __construct(UserAuthenticateService $userService, AuthValidate $authValidate)
    {
        $this->userAuthService = $userService;
        $this->authValidate = $authValidate;
    }

    public function authenticate(Request $request)
    {
        try {
            $credentials = $this->authValidate->valid($request);
            $result = $this->userAuthService->authUser($credentials['email'], $credentials['password']);
            return response()->json($result, 200);
        } catch (\InvalidArgumentException $e) {
            $error = json_decode($e->getMessage(), true);
            return response()->json([
                'message' => $error['message'],
                'fields' => $error['fields']
            ], $e->getCode());
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }
}
