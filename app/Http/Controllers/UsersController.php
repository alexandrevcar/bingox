<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\User as UserService;
use App\Services\User\Validate as UserValidate;

class UsersController extends Controller
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var UserValidate
     */
    protected $userValidate;

    /**
     * UsersController constructor.
     * @param UserService $userService
     * @param UserValidate $userValidate
     */
    public function __construct(UserService $userService, UserValidate $userValidate)
    {
        $this->userService = $userService;
        $this->userValidate = $userValidate;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $search = $request->search ?? null;
            $perPage = $request->per_page ?? 50;
            $user = $this->userService->list($perPage, $search);
            return response()->json($user, 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $user = $this->userService->get($id);
            $teste = $user->getAuthPassword();
            return response()->json($user->toArray(), 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $data = $this->userValidate->valid($request);
            $user = $this->userService->store($data);
            return response()->json($user->toArray(), 201);
        }catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this->userValidate->validUpdate($request);
            $user = $this->userService->update(
                $this->userService->get($id),
                $data
            );
            return response()->json($user->toArray(), 201);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->userService->destroy($this->userService->get($id));
            return response()->json([], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], $e->getCode() ?? 500);
        }
    }
}
