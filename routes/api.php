<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('jwt.auth')->group(function () {




    Route::prefix('cards')->group(function () {
        Route::get('/', 'CardsController@index');
        Route::post('/', 'CardsController@store');
        Route::get('/{id}', 'CardsController@show');
        Route::put('/{id}', 'CardsController@update');
        Route::delete('/{id}', 'CardsController@destroy');
    });

    Route::prefix('raffles')->group(function () {
        Route::get('/', 'RafflesController@index');
        Route::post('/', 'RafflesController@store');
        Route::get('/{id}', 'RafflesController@show');
        Route::put('/{id}', 'RafflesController@update');
        Route::delete('/{id}', 'RafflesController@destroy');
    });
});

Route::prefix('users')->group(function () {
    Route::get('/', 'UsersController@index');
    Route::post('/', 'UsersController@store');
    Route::get('/{id}', 'UsersController@show');
    Route::put('/{id}', 'UsersController@update');
    Route::delete('/{id}', 'UsersController@destroy');
});

Route::prefix('bars')->group(function () {
    Route::get('/', 'BarsController@index');
    Route::get('/{id}', 'BarsController@show');
    Route::put('/{id}', 'BarsController@update');
    Route::post('/', 'BarsController@store');
    Route::delete('/{id}', 'BarsController@destroy');
});

Route::prefix('rooms')->group(function () {
    Route::get('/', 'RoomsController@index');
    Route::get('/{id}', 'RoomsController@show');
    Route::put('/{id}', 'RoomsController@update');
    Route::post('/', 'RoomsController@store');
    Route::delete('/{id}', 'RoomsController@destroy');
});


Route::post('auth/login', 'AuthController@authenticate');
