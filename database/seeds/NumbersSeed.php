<?php

use Illuminate\Database\Seeder;

class NumbersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');

        $raffle = \App\Model\Raffle::all()->last();
        $collection = new \Illuminate\Support\Collection();
        foreach ($raffle->cards()->get() as $card) {
            $cardId = $card->id;

            for ($i = 1; $i <= 5; $i++) {
                $collection->add(new \App\Model\Card\Number([
                    'card_id' => $cardId,
                    'number' => $faker->randomNumber(2),
                    'column' => \App\Enums\ColumnsNumberTypeEnum::FIRST_COLUMN
                ]));
            }

            for ($i = 1; $i <= 5; $i++) {
                $collection->add(new \App\Model\Card\Number([
                    'card_id' => $cardId,
                    'number' => $faker->randomNumber(2),
                    'column' => \App\Enums\ColumnsNumberTypeEnum::SECOND_COLUMN
                ]));
            }

            for ($i = 1; $i <= 5; $i++) {
                $collection->add(new \App\Model\Card\Number([
                    'card_id' => $cardId,
                    'number' => $faker->randomNumber(2),
                    'column' => \App\Enums\ColumnsNumberTypeEnum::THIRD_COLUMN
                ]));
            }
        }

        $data = $collection->map(function ($item) {
            return [
                'card_id'=> $item->card_id,
                'number' => $item->number,
                'column' => $item->column,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        })->toArray();

        foreach (array_chunk($data, 500) as $dataChunked) {
            \App\Model\Card\Number::query()->insert($dataChunked);
        }
    }
}
