<?php

use Illuminate\Database\Seeder;

class RafflesNumbers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');

        $raffle = \App\Model\Raffle::all()->last();
        $rafflesNumbers = [];

        $hasKuadra = false;
        $hasKina = false;
        $hasKeno = false;

        $end = false;
        do {
            do {
                $randNumber = $faker->randomNumber(2);

                $hasNumber = \App\Model\Raffle\Number::where('number', $randNumber)
                    ->where('raffle_id', $raffle->id)
                    ->count();

            } while ($hasNumber);

            \App\Model\Raffle\Number::create([
                'raffle_id' => $raffle->id,
                'number' => $randNumber
            ]);

            $rafflesNumbers[] = $randNumber;

            $numberCards = \App\Model\Card\Number::orWhere(function($query) use ($rafflesNumbers) {
                $query->whereIn('number', $rafflesNumbers)
                    ->where('column', \App\Enums\ColumnsNumberTypeEnum::FIRST_COLUMN);
            })->orWhere(function($query) use ($rafflesNumbers) {
                $query->whereIn('number', $rafflesNumbers)
                    ->where('column', \App\Enums\ColumnsNumberTypeEnum::SECOND_COLUMN);
            })->orWhere(function($query) use ($rafflesNumbers) {
                $query->whereIn('number', $rafflesNumbers)
                    ->where('column', \App\Enums\ColumnsNumberTypeEnum::THIRD_COLUMN);
            })->groupBy(['card_id', 'column'])
                ->selectRaw('`card_id`, `column`, count(`id`) as total')->get();

            if(!$hasKuadra) {
                foreach ($numberCards as $numberCard) {
                    if ($numberCard->total == 4) {
                        \App\Model\Raffle\Prize::create([
                            'card_id' => $numberCard->card_id,
                            'raffle_id' => $raffle->id,
                            'prize' => $raffle->kuadra,
                            'type' => \App\Enums\PrizeTypeEnum::KUADRA
                        ]);
                        $hasKuadra = true;
                    }
                }
            }

            if(!$hasKina) {
                foreach ($numberCards as $numberCard) {
                    if ($numberCard->total == 5) {
                        \App\Model\Raffle\Prize::create([
                            'card_id' => $numberCard->card_id,
                            'raffle_id' => $raffle->id,
                            'prize' => $raffle->kina,
                            'type' => \App\Enums\PrizeTypeEnum::KINA
                        ]);
                        $hasKina = true;
                    }
                }
            }

            $numberCardsKeno = \App\Model\Card\Number::whereIn('number', $rafflesNumbers)
                ->groupBy(['card_id'])
                ->selectRaw('`card_id`, count(`id`) as total')->get();

            if(!$hasKeno) {
                foreach ($numberCardsKeno as $numberCard) {
                    if ($numberCard->total == 15) {
                        \App\Model\Raffle\Prize::create([
                            'card_id' => $numberCard->card_id,
                            'raffle_id' => $raffle->id,
                            'prize' => $raffle->keno,
                            'type' => \App\Enums\PrizeTypeEnum::KENO
                        ]);
                        $hasKeno = true;
                        $end = true;
                    }
                }
            }

        } while(!$end);
    }
}
