<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Database\Eloquent\Model::unguard();

        $this->call(UsersSeed::class);
        $this->call(BarsSeed::class);
        $this->call(RafflesSeed::class);
        $this->call(CardsSeed::class);
        $this->call(NumbersSeed::class);
//        $this->call(PrizesSeed::class);
        $this->call(RafflesNumbers::class);

        \Illuminate\Database\Eloquent\Model::reguard();
    }
}
