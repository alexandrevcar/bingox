<?php

use Illuminate\Database\Seeder;

class RafflesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');
        $user = \App\Model\User::where('type', \App\Enums\UserTypeEnum::ADMIN)->get()->last();
        \App\Model\Raffle::create([
            'user_id' => $user->id,
            'date' => $faker->dateTime,
            'kuadra' => number_format($faker->numberBetween(10, 15), 2, '.', ''),
            'kina' => number_format($faker->numberBetween(15, 50), 2, '.', ''),
            'keno' => number_format($faker->numberBetween(50, 1000), 2, '.', ''),
            'status' => \App\Enums\StatusEnum::ACTIVE
        ]);
    }
}
