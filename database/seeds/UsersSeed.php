<?php

use Illuminate\Database\Seeder;

class UsersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');
        \App\Model\User::create([
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => bcrypt('123456'),
            'type' => \App\Enums\UserTypeEnum::ADMIN,
            'status' => \App\Enums\StatusEnum::ACTIVE
        ]);
    }
}
