<?php

use Illuminate\Database\Seeder;

class BarsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ufs = \App\Enums\StatesEnum::toArray();
        $faker = Faker\Factory::create('pt_BR');
        $user = \App\Model\User::where('type', \App\Enums\UserTypeEnum::ESTABLISHMENT)->get()->last();
        \App\Model\Bar::create([
            'user_id' => $user->id,
            'name' => "Bar do {$faker->name}",
            'postcode' => $faker->postcode,
            'address' => $faker->address,
            'number' => $faker->randomNumber(5),
            'neighborhood' => $faker->name,
            'city' => $faker->city,
            'state' =>  $ufs[$faker->randomKey($ufs)],
            'status' => \App\Enums\StatusEnum::ACTIVE
        ]);
    }
}
