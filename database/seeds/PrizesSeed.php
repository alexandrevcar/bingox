<?php

use Illuminate\Database\Seeder;

class PrizesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $raffle = \App\Model\Raffle::all()->last();

        \App\Model\Raffle\Prize::create([
            'card_id' => $raffle->cards()->get()->random()->id,
            'raffle_id' => $raffle->id,
            'prize' => $raffle->kuadra,
            'type' => \App\Enums\PrizeTypeEnum::KUADRA
        ]);

        \App\Model\Raffle\Prize::create([
            'card_id' => $raffle->cards()->get()->random()->id,
            'raffle_id' => $raffle->id,
            'prize' => $raffle->kina,
            'type' => \App\Enums\PrizeTypeEnum::KINA
        ]);

        \App\Model\Raffle\Prize::create([
            'card_id' => $raffle->cards()->get()->random()->id,
            'raffle_id' => $raffle->id,
            'prize' => $raffle->keno,
            'type' => \App\Enums\PrizeTypeEnum::KENO
        ]);
    }
}
