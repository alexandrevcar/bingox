<?php

use Illuminate\Database\Seeder;

class CardsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $raffle = \App\Model\Raffle::all()->last();
        $collection = new \Illuminate\Support\Collection();
        for($i=0; $i<100; $i++) {
            $collection->add(new \App\Model\Card([
                'bar_id' => \App\Model\Bar::all()->random()->id,
                'raffle_id' => $raffle->id,
                'status' => \App\Enums\StatusEnum::ACTIVE
            ]));
        }

        $data = $collection->map(function ($item) {
            return [
                'bar_id'=> $item->bar_id,
                'raffle_id' => $item->raffle_id,
                'status' => $item->status,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        })->toArray();

        foreach (array_chunk($data, 500) as $dataChunked) {
            \App\Model\Card::query()->insert($dataChunked);
        }
    }
}
