<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnStatusIdAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bars', function (Blueprint $table) {
            $table->dropForeign('bars_status_id_foreign');
            $table->dropColumn('status_id');
        });

        Schema::table('cards', function (Blueprint $table) {
            $table->dropForeign('cards_status_id_foreign');
            $table->dropColumn('status_id');
        });

        Schema::table('raffles', function (Blueprint $table) {
            $table->dropForeign('raffles_status_id_foreign');
            $table->dropColumn('status_id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_status_id_foreign');
            $table->dropColumn('status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
