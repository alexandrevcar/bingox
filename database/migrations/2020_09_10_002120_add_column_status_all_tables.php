<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bars', function (Blueprint $table) {
            $table->addColumn('integer', 'status');
        });

        Schema::table('cards', function (Blueprint $table) {
            $table->addColumn('integer', 'status');
        });

        Schema::table('raffles', function (Blueprint $table) {
            $table->addColumn('integer', 'status');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->addColumn('integer', 'status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
