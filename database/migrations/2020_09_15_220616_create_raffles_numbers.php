<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRafflesNumbers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raffles_numbers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('raffle_id')->unsigned();
            $table->foreign('raffle_id')
                ->references('id')
                ->on('raffles');
            $table->integer('number')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raffles_numbers');
    }
}
