<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTypeUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('type');
        });

        $types = array_values(\App\Enums\UserTypeEnum::toArray());
        $strTypes = implode("','", $types);
        \Illuminate\Support\Facades\DB::statement(sprintf("ALTER TABLE users add COLUMN type ENUM('%s') NOT NULL;", $strTypes));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
