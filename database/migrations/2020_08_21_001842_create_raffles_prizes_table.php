<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRafflesPrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raffles_prizes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('card_id')->unsigned();
            $table->foreign('card_id')
                ->references('id')
                ->on('cards');
            $table->bigInteger('raffle_id')->unsigned();
            $table->foreign('raffle_id')
                ->references('id')
                ->on('raffles');
            $table->double('prize');
            $table->enum('type', array_values(\App\Enums\PrizeTypeEnum::toArray()));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raffles_prizes');
    }
}
