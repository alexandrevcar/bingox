import Index from "./view/Index";
import MyAccount from "./view/MyAccount";
import Profile from "./view/examples/Profile";
import Maps from "./view/examples/Maps";
import Login from "./view/Login";
import Tables from "./view/examples/Tables";
import Icons from "./view/examples/Icons";
import IndexEstablishments from "./view/establishments/Index";
import CreateEstablishments from "./view/establishments/Create";
import IndexRooms from "./view/rooms/Index";
import CreateRooms from "./view/rooms/Create";
import IndexRaffles from "./view/raffles/Index";

var routes = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/admin"
  },
  {
    path: "/my-account",
    name: "Minha Conta",
    icon: "ni ni-single-02 text-yellow",
    component: MyAccount,
    layout: "/admin"
  },
  {
    path: "/establishments/create",
    name: "Estabelecimentos",
    icon: "ni ni-building text-blue",
    hide: true,
    component: CreateEstablishments,
    layout: "/admin"
  },
  {
    path: "/establishments/:id",
    name: "Estabelecimentos",
    hide: true,
    icon: "ni ni-building text-blue",
    component: CreateEstablishments,
    layout: "/admin"
  },
  {
    path: "/establishments",
    name: "Estabelecimentos",
    icon: "ni ni-building text-blue",
    component: IndexEstablishments,
    layout: "/admin"
  },
  {
    path: "/rooms/create",
    name: "Salas",
    icon: "ni ni-planet text-red",
    hide: true,
    component: CreateRooms,
    layout: "/admin"
  },
  {
    path: "/rooms/:id",
    name: "Salas",
    hide: true,
    icon: "ni ni-planet text-red",
    component: CreateRooms,
    layout: "/admin"
  },
  {
    path: "/rooms",
    name: "Salas",
    icon: "ni ni-planet text-red",
    component: IndexRooms,
    layout: "/admin"
  },
  {
    path: "/raffles",
    name: "Sorteios",
    icon: "ni ni-bulb-61 text-orange",
    component: IndexRaffles,
    layout: "/admin"
  },
  {
    path: "/icons",
    name: "Icons",
    icon: "ni ni-planet text-blue",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/maps",
    name: "Maps",
    icon: "ni ni-pin-3 text-orange",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/user-profile",
    name: "User Profile",
    icon: "ni ni-single-02 text-yellow",
    component: Profile,
    layout: "/admin"
  },
  {
    path: "/tables",
    name: "Tables",
    icon: "ni ni-bullet-list-67 text-red",
    component: Tables,
    layout: "/admin"
  },
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: Login,
    layout: "/auth"
  }
];
export default routes;
