import Axios from 'axios';

export const rootUrl = 'http://bingox.local/';
export const rootUrlAdmin = 'http://bingox.local/admin/';

export const Http = Axios.create({
    baseUrl: rootUrl
})