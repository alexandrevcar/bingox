import { changeLoading } from '../../store/actions/loading.action';
import { changeNotify } from '../../store/actions/notify.action';
import api from '../../store/actions/api';

export const ACTION_TYPES = {
    FETCH_ALL_USERS: 'FETCH_ALL_USERS',
}

export const fetchAll = (params, callback = null) => dispatch => {
    api.users().fetchAll(params)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.FETCH_ALL_USERS,
                payload: {
                    pagination: {
                        page: res.data.current_page,
                        to: res.data.to,
                        total: res.data.total,
                        per_page: res.data.per_page,
                        data: res.data.data
                    }
                }
            })
            if(callback) callback()
        })
        .catch(error => {
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if (typeof error.response != null) {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: error.response.data.message
                }))
            } else {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: 'Erro ao carregar os usuários'
                }))
            }
            if(callback) callback()
        });
}