import { changeLoading } from '../../store/actions/loading.action';
import { changeNotify } from '../../store/actions/notify.action';
import api from '../../store/actions/api';

export const ACTION_TYPES = {
    FETCH_ALL_ROOMS: 'FETCH_ALL_ROOMS',
}

export const fetchAll = (params, callback = null) => dispatch => {
    api.rooms().fetchAll(params)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.FETCH_ALL_ROOMS,
                payload: {
                    pagination: {
                        page: res.data.current_page,
                        to: res.data.to,
                        total: res.data.total,
                        per_page: res.data.per_page,
                        data: res.data.data
                    }
                }
            })
            if(callback) callback()
        })
        .catch(error => {
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if (typeof error.response != "undefined") {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: error.response.data.message
                }))
            } else {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: 'Erro ao carregar as salas'
                }))
            }
            if(callback) callback()
        });
}

export const create = (data, callback = null) => dispatch => {
    dispatch(changeLoading({
        open: true,
        msg: 'Salvando ... '
    }));
    api.rooms().create(data)
        .then(res => {
            if (typeof res !== 'undefined') {
                dispatch(changeNotify({
                    open: true,
                    error: false,
                    message: 'Criado com sucesso'
                }));
            }
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if(callback) callback();
        }).catch(error => {
            let fields = [];
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if (typeof error.response != "undefined") {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: error.response.data.message
                }))
                fields = (typeof error.response.data.fields != "undefined") ? error.response.data.fields : [];
                if(callback) callback(fields)
            } else {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: 'Erro ao salvar a sala'
                }))
            }
        })

}

export const update = (id, data, callback = null) => dispatch => {
    dispatch(changeLoading({
        open: true,
        msg: 'Salvando ... '
    }));
    api.rooms().update(id, data)
        .then(res => {
            if (typeof res !== 'undefined') {
                dispatch(changeNotify({
                    open: true,
                    error: false,
                    message: 'Atualizado com sucesso'
                }));
            }
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if(callback) callback();
        }).catch(error => {
            let fields = [];
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if (typeof error.response != "undefined") {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: error.response.data.message
                }))
                fields = (typeof error.response.data.fields != "undefined") ? error.response.data.fields : [];
                if(callback) callback(fields);
            } else {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: 'Erro ao salvar a sala'
                }))
            }
        })

}

export const get = (id, callback= null) => dispatch => {
    dispatch(changeLoading({
        open: true,
        msg: 'Carregando ... '
    }));
    api.rooms().fetchById(id)
        .then(res => {
            if(typeof res !== 'undefined') {
                if(callback) callback(res.data);
            }
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
        }).catch(error => {
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if (typeof error.response != "undefined") {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: error.response.data.message
                }))
            } else {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: 'Erro ao pegar a sala'
                }))
            }
        })

}

export const remove = (id, callback) => dispatch => {
    dispatch(changeLoading({
        open: true,
        msg: 'Carregando ... '
    }));
    api.rooms().remove(id)
        .then(res => {
            if (typeof res !== 'undefined') {
                dispatch(changeNotify({
                    open: true,
                    error: false,
                    message: 'Removido com sucesso'
                }));
            }
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            callback();
        }).catch(error => {
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if (typeof error.response != "undefined") {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: error.response.data.message
                }))
            } else {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: 'Erro ao pegar a sala'
                }))
            }
            callback();
        })

}