import { changeLoading } from '../../store/actions/loading.action';
import { changeNotify } from '../../store/actions/notify.action';
import api from "./api";

export const actionTypes = {
    GET_TOKEN : 'GET_TOKEN',
    SET_TOKEN: 'SET_TOKEN',
    SET_USER: 'SET_USER',
    GET_USER: 'GET_USER'
}

export const login = (data, callback) => dispatch => {
    dispatch(changeLoading({
        open: true,
        msg: 'Autenticando ... '
    }));
    api.auth().login(data)
        .then(res => {
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if (typeof res !== 'undefined') {
                dispatch(setUserToken(res.data.access_token))
                dispatch(setUserData(res.data.user))
            }
            callback(true)
        }).catch(error => {
            dispatch(changeLoading({
                open: false,
                msg: ''
            }));
            if (typeof error.response != null) {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: error.response.data.message
                }))
                let fields = (typeof error.response.data.fields != "undefined") ? error.response.data.fields : [];
                callback(false, fields);
            } else {
                dispatch(changeNotify({
                    open: true,
                    error: true,
                    message: 'Erro ao efetuar login'
                }))
            }
        })

}

export const getToken = (token) => ({
    type: actionTypes.GET_TOKEN,
    token
})

export const getUserToken = () => dispach =>
    localStorage.getItem('access.token').then(res => {
        if(typeof res !== 'undefined') {
            dispach(getToken(res));
        }
    })

export const setUserToken = (token) => {
    return function(dispatch, getState) {
        localStorage.setItem('access.token', token);
        return {
            type: actionTypes.SET_TOKEN,
            payload: true
        }
    }
}

export const setUserData = (user) => {
    return function(dispatch, getState) {
        let userString = JSON.stringify(user);
        localStorage.setItem('access.user', userString);
        return {
            type: actionTypes.SET_USER,
            payload: true
        }
    }
}

export const getUserData = () => {
    let user = JSON.parse(localStorage.getItem('access.user'));
    return {
        type: actionTypes.GET_USER,
        user
    }
}