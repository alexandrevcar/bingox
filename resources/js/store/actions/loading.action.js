export const actionTypes = {
    OPEN : 'OPEN'
}

export const changeLoading = (payload) => ({
    type: actionTypes.OPEN,
    payload
})