import Axios from 'axios';

export const baseUrl = 'http://bingox.local/api/';
export const baseUrlAdmin = 'http://bingox.local/admin/';

export default {
    establishments(url = baseUrl+'bars/')
    {
        return {
            fetchAll: (search) => Axios.get(url, {params: search}),
            fetchById: id => Axios.get(url+id),
            create: newRecord => Axios.post(url, newRecord),
            update: (id, updateRecord) => Axios.put(url+id, updateRecord),
            remove: id => Axios.delete(url+id)
        }
    },
    rooms(url = baseUrl+'rooms/')
    {
        return {
            fetchAll: (search) => Axios.get(url, {params: search}),
            fetchById: id => Axios.get(url+id),
            create: newRecord => Axios.post(url, newRecord),
            update: (id, updateRecord) => Axios.put(url+id, updateRecord),
            remove: id => Axios.delete(url+id)
        }
    },
    users(url = baseUrl+'users/')
    {
        return {
            fetchAll: (search) => Axios.get(url, {params: search}),
            fetchById: id => Axios.get(url+id),
            create: newRecord => Axios.post(url, newRecord),
            update: (id, updateRecord) => Axios.put(url+id, updateRecord),
            remove: id => Axios.delete(url+id)
        }
    },
    auth(url = baseUrl+'auth/login/')
    {
        return {
            login: (credencials) => Axios.post(url, credencials)
        }
    }
}