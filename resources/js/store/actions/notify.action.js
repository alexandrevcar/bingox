export const actionTypes = {
    CHANGE : 'CHANGE'
}

export const changeNotify = (payload) => ({
    type: actionTypes.CHANGE,
    payload
})