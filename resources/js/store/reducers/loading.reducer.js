import initialState from './initialState'
import {actionTypes} from "../actions/loading.action";

const loadingReducer = (state = initialState.loading, action) => {
    switch (action.type) {
        case actionTypes.OPEN:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}

export  default loadingReducer;
