import { combineReducers } from 'redux';
import authReducer from './auth.reducer';
import notifyReducer from './notify.reducer';
import establishmentsReducer from './establishments.reducer';
import loadingReducer from './loading.reducer';
import statesReducer from './states.reducer';
import usersReducer from './users.reducer';
import roomsReducer from './rooms.reducer';

const rootReducer = combineReducers({
    authReducer,
    notifyReducer,
    establishmentsReducer,
    loadingReducer,
    statesReducer,
    usersReducer,
    roomsReducer
})

export default rootReducer;