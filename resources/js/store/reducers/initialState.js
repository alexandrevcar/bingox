import React from "react";

export default {
    auth: {
        token: {},
        user: {},
        credentials: {
            email: '',
            password: ''
        },
        success: false,
        error: {}
    },
    loading: {
        open: false,
        msg: null
    },
    notify: {
        open: false,
        time: 6000,
        message: 'Open',
        error: false
    },
    establishments: {
        pagination: {
            page: 1,
            current_page: 1,
            to: 1,
            total: 1,
            per_page: 2,
            data: []
        }
    },
    rooms: {
        pagination: {
            page: 1,
            current_page: 1,
            to: 1,
            total: 1,
            per_page: 2,
            data: []
        }
    },
    users: {
        pagination: {
            page: 1,
            current_page: 1,
            to: 1,
            total: 1,
            per_page: 2,
            data: []
        },
    },
    states: [
        {code: "AC", name: "Acre"},
        {code: "AL", name: "Alagoas"},
        {code: "AP", name: "Amapá"},
        {code: "AM", name: "Amazonas"},
        {code: "BA", name: "Bahia"},
        {code: "CE", name: "Ceará"},
        {code: "DF", name: "Distrito Federal"},
        {code: "ES", name: "Espírito Santo"},
        {code: "GO", name: "Goiás"},
        {code: "MA", name: "Maranhão"},
        {code: "MT", name: "Mato Grosso"},
        {code: "MS", name: "Mato Grosso do Sul"},
        {code: "MG", name: "Minas Gerais"},
        {code: "PA", name: "Pará"},
        {code: "PB", name: "Paraíba"},
        {code: "PR", name: "Paraná"},
        {code: "PE", name: "Pernambuco"},
        {code: "PI", name: "Piauí"},
        {code: "RJ", name: "Rio de Janeiro"},
        {code: "RN", name: "Rio Grande do Norte"},
        {code: "RS", name: "Rio Grande do Sul"},
        {code: "RO", name: "Rondônia"},
        {code: "RR", name: "Roraima"},
        {code: "SC", name: "Santa Catarina"},
        {code: "SP", name: "São Paulo"},
        {code: "SE", name: "Sergipe"},
        {code: "TO", name: "Tocantins"}
    ]
}