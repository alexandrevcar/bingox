import { actionTypes } from "../actions/auth.action";
import initialState from './initialState'

const authReducer = (state = initialState.auth, action) => {
    switch (action.type) {
        case actionTypes.GET_TOKEN:
            return {
                ...state,
                ...action.token
            }
        case actionTypes.SET_USER:
            return {
                ...state,
                ...action.user
            }
        case actionTypes.GET_USER:
            return {
                ...state,
                user: {
                    ...state.user,
                    ...action.user
                }
            }
        default:
            return state;
    }
}

export  default authReducer;
