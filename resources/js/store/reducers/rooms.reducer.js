import initialState from './initialState'
import { ACTION_TYPES } from "../actions/rooms.action";

const roomsReducer = (state = initialState.rooms, action) => {
    switch (action.type) {
        case ACTION_TYPES.FETCH_ALL_ROOMS:
            console.log(action.payload, 'reduce');
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}

export  default roomsReducer;
