import initialState from './initialState'
import {ACTION_TYPES} from "../actions/users.action";

const usersReducer = (state = initialState.users, action) => {
    switch (action.type) {
        case ACTION_TYPES.FETCH_ALL_USERS:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}


export  default usersReducer;
