import initialState from './initialState'
import { ACTION_TYPES } from "../actions/establishments.action";

const establishmentsReducer = (state = initialState.establishments, action) => {
    switch (action.type) {
        case ACTION_TYPES.FETCH_ALL_ESTABLISHMENTS:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}

export  default establishmentsReducer;
