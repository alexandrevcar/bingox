import { actionTypes } from "../actions/notify.action";
import initialState from './initialState'

const notifyReducer = (state = initialState.notify, action) => {
    switch (action.type) {
        case actionTypes.CHANGE:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }

}

export  default notifyReducer;
