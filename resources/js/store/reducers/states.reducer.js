import initialState from './initialState'

const statesReducer = (state = initialState.states, action) => {
    switch (action.type) {
        default:
            return state;
    }
}

export  default statesReducer;
