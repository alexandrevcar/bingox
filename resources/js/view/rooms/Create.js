import React, {useEffect} from "react";
import useForm from "../../components/useForm";
import {create, update, get} from "../../store/actions/rooms.action";
import {fetchAll} from "../../store/actions/users.action";
import {connect} from "react-redux";

// reactstrap components
import {
    Card,
    CardHeader,
    CardBody,
    Container,
    Row,
    Col,
    FormGroup,
    Button
} from "reactstrap";

import {
    TextField,
    FormHelperText,
} from "@material-ui/core";

import Async from "react-select/async/dist/react-select.esm";

import Header from "../../components/Headers/Header";
import {baseUrlAdmin} from "../../store/actions/api";

const initialFieldValues = {
    id: '',
    user_id: '',
    name: ''
}

const CreateEstablishments = (props) => {

    useEffect(() => {
        if(props.match.params.id) {
            props.get(props.match.params.id, function(data) {
                if(typeof data != "undefined") {
                    setValues(data)
                    setUser({
                        label: data.user.name,
                        value: data.user.id
                    });
                }
            });
        }
    } ,[]);

    const getDataUsers = (value, callback) => {
        let users = props.users.pagination.data.filter(i => {
            return i.name.toLowerCase().includes(value.toLowerCase());
        });
        console.log(users, 'getUsers');
        let result = [];
        users.forEach((user) => {
            result.push({
                label: user.name,
                value: user.id
            })
        });
        callback(result);
    }

   const searchUsers = (inputValue, callback) => {
        setTimeout(() => {
            props.fetchAllUsers({search: inputValue}, function () {
                getDataUsers(inputValue, callback);
            })
        }, 1000);
    }

    const validate = () => {
        let temp = {...errors}
        temp.user_id = values.user_id?"":"This user field is required";
        temp.name = values.name?"":"This name field is required";
        setErrors({
            ...temp
        })
        return Object.values(temp).every(x => x=="");
    }

    let {
        values,
        setValues,
        errors,
        setErrors,
        user,
        setUser,
        room,
        setRoom,
        handleInputChange
    } = useForm(initialFieldValues)

    const onSuccess = () => {
        window.location.replace(baseUrlAdmin + 'rooms');
    }

    const onError = (errors) => {
        setErrors({
            ...errors
        })
    }

    const onSend = (errors) => {
        if(typeof errors != 'undefined') {
            onError(errors)
        } else {
            onSuccess();
        }
    }

    const handleSubmit = e => {
        e.preventDefault();
        if(validate()) {
            if(values.id) {
                props.update(values.id, values, onSend);
            } else {
                props.create(values, onSend);
            }
        }
    }

    return (
        <>
            <Header />
            <Container className="mt--7" fluid>
                <Row>
                    <Col className="order-xl-1" xl="12">
                        <Card className="bg-secondary shadow">
                            <CardHeader className="bg-white border-0">
                                <Row className="align-items-center">
                                    <Col xs="8">
                                        <h3 className="mb-0">
                                            Sala
                                        </h3>
                                    </Col>
                                </Row>
                            </CardHeader>
                            <CardBody>
                                <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                                    <h6 className="heading-small text-muted mb-4">
                                        Informação
                                    </h6>
                                    <div className="pl-lg-4">
                                        <Row>
                                            <Col lg="12">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="name"
                                                    >
                                                        Responsável
                                                    </label>

                                                    <Async
                                                        value={user}
                                                        cacheOptions
                                                        loadOptions={searchUsers}
                                                        fullWidth
                                                        variant="outlined"
                                                        name="user_id"
                                                        id="user_id"
                                                        onChange={(text) => handleInputChange({type: 'user', value: text.value, label: text.label})}
                                                        {...(errors.user_id && {error:true})}
                                                    />
                                                </FormGroup>
                                                {(errors.user_id &&
                                                    <FormHelperText className="text-red">{errors.user_id}</FormHelperText>
                                                )}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg="12">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="name"
                                                    >
                                                        Nome
                                                    </label>
                                                    <TextField
                                                        name="name"
                                                        variant="outlined"
                                                        fullWidth
                                                        value={values.name}
                                                        onChange={handleInputChange}
                                                        {...(errors.name && {error:true,helperText:errors.name})}
                                                        />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg="12">
                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    size="large"
                                                    type="submit"
                                                >
                                                    Salvar
                                                </Button>
                                            </Col>
                                        </Row>
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
}


const mapStateToProps = (state) => ({
    rooms: state.roomsReducer,
    users: state.usersReducer
})

const mapDispatchToProps = (dispatch) => ({
    create: (newRecord, onSend) => dispatch(create(newRecord, onSend)),
    update: (id, record, onSend) => dispatch(update(id, record, onSend)),
    get: (id, setValues) => dispatch(get(id, setValues)),
    fetchAllUsers: (params, getDataUsers, callback) => dispatch(fetchAll(params, getDataUsers, callback))
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateEstablishments);