import React, {useEffect} from "react";
import useForm from "../../components/useForm";
import {create, update, get} from "../../store/actions/establishments.action";
import {fetchAll as fetchAllUsers} from "../../store/actions/users.action";
import {fetchAll as fetchAllRooms} from "../../store/actions/rooms.action";
import Cep from "../../components/Inputs/Cep";
import {connect} from "react-redux";

// reactstrap components
import {
    Card,
    CardHeader,
    CardBody,
    Container,
    Row,
    Col,
    FormGroup,
    Button
} from "reactstrap";

import {
    Select,
    TextField,
    FormHelperText,
    MenuItem
} from "@material-ui/core";

import Async from "react-select/async/dist/react-select.esm";

import Header from "../../components/Headers/Header";
import {baseUrlAdmin} from "../../store/actions/api";

const initialFieldValues = {
    id: '',
    user_id: '',
    room_id: '',
    name: '',
    postcode: '',
    address: '',
    number: '',
    neighborhood: '',
    city: '',
    state: ''
}



const CreateEstablishments = (props) => {

    useEffect(() => {
        if(props.match.params.id) {
            props.get(props.match.params.id, function(data) {
                if(typeof data != "undefined") {
                    setValues(data)
                    setUser({
                        label: data.user.name,
                        value: data.user.id
                    });
                    setRoom({
                        label: data.room.name,
                        value: data.room.id
                    });
                }
            });
        }
    } ,[]);

    const getDataUsers = (value, callback) => {
        let users = props.users.pagination.data.filter(i => {
            return i.name.toLowerCase().includes(value.toLowerCase());
        });
        let result = [];
        users.forEach((user) => {
            result.push({
                label: user.name,
                value: user.id
            })
        });
        callback(result);
    }

    const getDataRooms = (value, callback) => {
        let rooms = props.rooms.pagination.data.filter(i => {
            return i.name.toLowerCase().includes(value.toLowerCase());
        });
        let result = [];
        rooms.forEach((room) => {
            result.push({
                label: room.name,
                value: room.id
            })
        });
        callback(result);
    }

   const searchUsers = (inputValue, callback) => {
        setTimeout(() => {
            props.fetchAllUsers({search: inputValue}, function () {
                getDataUsers(inputValue, callback);
            })
        }, 1000);
    }

    const searchRooms = (inputValue, callback) => {
        setTimeout(() => {
            props.fetchAllRooms({search: inputValue}, function () {
                getDataRooms(inputValue, callback);
            })
        }, 1000);
    }

    const validate = () => {
        let temp = {...errors}
        temp.user_id = values.user_id?"":"This user field is required";
        temp.room_id = values.room_id?"":"This room field is required";
        temp.name = values.name?"":"This name field is required";
        let regexpCep = /^[0-9]{5}-[0-9]{3}$/;
        temp.postcode = values.postcode?"":"This postcode field is required";
        if(!regexpCep.test(values.postcode)) {
            temp.postcode = "This postcode invalid";
        }
        temp.address = values.address?"":"This address field is required";
        temp.number = values.number?"":"This number field is required";
        temp.neighborhood = values.neighborhood?"":"This neighborhood field is required";
        temp.city = values.city?"":"This city field is required";
        temp.state = values.state?"":"This state field is required";
        setErrors({
            ...temp
        })
        return Object.values(temp).every(x => x=="");
    }

    let {
        values,
        setValues,
        errors,
        setErrors,
        user,
        setUser,
        room,
        setRoom,
        handleInputChange
    } = useForm(initialFieldValues)

    const onSuccess = () => {
        window.location.replace(baseUrlAdmin + 'establishments');
    }

    const onError = (errors) => {
        setErrors({
            ...errors
        })
    }

    const onSend = (errors) => {
        if(typeof errors != 'undefined') {
            onError(errors)
        } else {
            onSuccess();
        }
    }

    const handleSubmit = e => {
        e.preventDefault();
        if(validate()) {
            if(values.id) {
                props.update(values.id, values, onSend);
            } else {
                props.create(values, onSend);
            }
        }
    }

    return (
        <>
            <Header />
            <Container className="mt--7" fluid>
                <Row>
                    <Col className="order-xl-1" xl="12">
                        <Card className="bg-secondary shadow">
                            <CardHeader className="bg-white border-0">
                                <Row className="align-items-center">
                                    <Col xs="8">
                                        <h3 className="mb-0">
                                            Estabelecimento
                                        </h3>
                                    </Col>
                                </Row>
                            </CardHeader>
                            <CardBody>
                                <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                                    <h6 className="heading-small text-muted mb-4">
                                        Informação
                                    </h6>
                                    <div className="pl-lg-4">
                                        <Row>
                                            <Col lg="12">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="name"
                                                    >
                                                        Sala
                                                    </label>

                                                    <Async
                                                        value={room}
                                                        cacheOptions
                                                        loadOptions={searchRooms}
                                                        fullWidth
                                                        variant="outlined"
                                                        name="room_id"
                                                        id="room_id"
                                                        onChange={(text) => handleInputChange({type: 'room', value: text.value, label: text.label})}
                                                        {...(errors.room_id && {error:true})}
                                                    />
                                                </FormGroup>
                                                {(errors.room_id &&
                                                    <FormHelperText className="text-red">{errors.room_id}</FormHelperText>
                                                )}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg="12">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="user_id"
                                                    >
                                                        Responsável
                                                    </label>

                                                    <Async
                                                        value={user}
                                                        cacheOptions
                                                        loadOptions={searchUsers}
                                                        fullWidth
                                                        variant="outlined"
                                                        name="user_id"
                                                        id="user_id"
                                                        onChange={(text) => handleInputChange({type: 'user', value: text.value, label: text.label})}
                                                        {...(errors.user_id && {error:true})}
                                                    />
                                                </FormGroup>
                                                {(errors.user_id &&
                                                    <FormHelperText className="text-red">{errors.user_id}</FormHelperText>
                                                )}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg="12">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="name"
                                                    >
                                                        Nome
                                                    </label>
                                                    <TextField
                                                        name="name"
                                                        variant="outlined"
                                                        fullWidth
                                                        value={values.name}
                                                        onChange={handleInputChange}
                                                        {...(errors.name && {error:true,helperText:errors.name})}
                                                        />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg="3">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="postcode"
                                                    >
                                                        CEP
                                                    </label>
                                                    <TextField
                                                        name="postcode"
                                                        variant="outlined"
                                                        fullWidth
                                                        onChange={handleInputChange}
                                                        value={values.postcode}
                                                        {...(errors.postcode && {error:true,helperText:errors.postcode})}
                                                        InputProps={{
                                                            inputComponent: Cep

                                                        }}
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col lg="6">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="address"
                                                    >
                                                        Endereço
                                                    </label>
                                                    <TextField
                                                        name="address"
                                                        variant="outlined"
                                                        fullWidth
                                                        placeholder="Endereço"
                                                        value={values.address}
                                                        onChange={handleInputChange}
                                                        {...(errors.address && {error:true,helperText:errors.address})}
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col lg="3">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="number"
                                                    >
                                                        Número
                                                    </label>
                                                    <TextField
                                                        name="number"
                                                        variant="outlined"
                                                        fullWidth
                                                        placeholder="Número"
                                                        value={values.number}
                                                        onChange={handleInputChange}
                                                        {...(errors.number && {error:true,helperText:errors.number})}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg="5">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="neighborhood"
                                                    >
                                                        Bairro
                                                    </label>
                                                    <TextField
                                                        name="neighborhood"
                                                        variant="outlined"
                                                        fullWidth
                                                        placeholder="Bairro"
                                                        value={values.neighborhood}
                                                        onChange={handleInputChange}
                                                        {...(errors.neighborhood && {error:true,helperText:errors.neighborhood})}
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col lg="5">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="city"
                                                    >
                                                        Cidade
                                                    </label>
                                                    <TextField
                                                        name="city"
                                                        variant="outlined"
                                                        fullWidth
                                                        placeholder="Cidade"
                                                        value={values.city}
                                                        onChange={handleInputChange}
                                                        {...(errors.city && {error:true,helperText:errors.city})}
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col lg="2">
                                                <FormGroup>
                                                    <label
                                                        className="form-control-label"
                                                        htmlFor="state"
                                                    >
                                                        UF
                                                    </label>
                                                    <Select
                                                        name="state"
                                                        id="state"
                                                        variant="outlined"
                                                        fullWidth
                                                        value={values.state}
                                                        defaultValue={values.state}
                                                        onChange={handleInputChange}
                                                        {...(errors.state && {error:true})}
                                                    >
                                                        {props.states.map((uf) =>
                                                            <MenuItem key={uf.code} value={uf.code}>{uf.name}</MenuItem>
                                                        )}
                                                    </Select>
                                                    {(errors.state &&
                                                        <FormHelperText className="text-red">{errors.state}</FormHelperText>
                                                    )}
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg="12">
                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    size="large"
                                                    type="submit"
                                                >
                                                    Salvar
                                                </Button>
                                            </Col>
                                        </Row>
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
}


const mapStateToProps = (state) => ({
    establishments: state.establishmentsReducer,
    states: state.statesReducer,
    users: state.usersReducer,
    rooms: state.roomsReducer
})

const mapDispatchToProps = (dispatch) => ({
    create: (newRecord, onSend) => dispatch(create(newRecord, onSend)),
    update: (id, record, onSend) => dispatch(update(id, record, onSend)),
    get: (id, setValues) => dispatch(get(id, setValues)),
    fetchAllUsers: (params, getDataUsers, callback) => dispatch(fetchAllUsers(params, getDataUsers, callback)),
    fetchAllRooms: (params, getDataRooms, callback) => dispatch(fetchAllRooms(params, getDataRooms, callback))
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateEstablishments);