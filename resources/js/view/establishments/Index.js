import React from "react";
import { Link } from "react-router-dom";

// reactstrap components
import {
    Card,
    CardHeader,
    CardFooter,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    DropdownToggle,
    Table,
    Container,
    Row, Button, Col,
} from "reactstrap";
// core components
import Header from "../../components/Headers/Header";
import { connect } from 'react-redux';
import {fetchAll, remove} from '../../store/actions/establishments.action';
import Paginations from "../../components/Paginations/Paginations";

const Index = (props) => {
  const remove = (id) => {
    props.remove(id, function () {
      props.fetchAll({
        page: props.establishments.pagination.page,
        per_page: props.establishments.pagination.per_page
      });
    })
  }

  return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0">
                    <Row>
                        <Col lg="11">
                            <h3 className="mb-0">Estabelecimentos {process.env.MIX_SENTRY_DSN_PUBLIC}</h3>
                        </Col>
                        <Col lg="1">
                            <Button
                                variant="contained"
                                color="danger"
                                size="large"
                                type="button"
                                to="/admin/establishments/create"
                                tag={Link}
                            >
                                Novo
                            </Button>
                        </Col>
                    </Row>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                  <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Responsável</th>
                    <th scope="col">Sala</th>
                    <th scope="col">CEP</th>
                    <th scope="col">Cidade</th>
                    <th scope="col">UF</th>
                    <th scope="col">Status</th>
                    <th scope="col" />
                  </tr>
                  </thead>
                  <tbody>
                  {props.establishments.pagination.data.map(establishment =>
                      <tr key={establishment.id}>
                        <th scope="row">{establishment.name}</th>
                        <td>{establishment.user.name}</td>
                        <td>{establishment.room.name}</td>
                        <td>{establishment.postcode}</td>
                        <td>{establishment.city}</td>
                        <td>{establishment.state}</td>
                        <td>{establishment.statusLabel}</td>
                        <td className="text-right">
                          <UncontrolledDropdown>
                            <DropdownToggle
                                className="btn-icon-only text-light"
                                href="#pablo"
                                role="button"
                                size="sm"
                                color=""
                                onClick={e => e.preventDefault()}
                            >
                              <i className="fas fa-ellipsis-v" />
                            </DropdownToggle>
                            <DropdownMenu className="dropdown-menu-arrow" right>
                              <DropdownItem
                                  to={establishment.id && '/admin/establishments/'+establishment.id}
                                  tag={Link}
                              >
                                Editar
                              </DropdownItem>
                              <DropdownItem
                                  onClick={e => remove(establishment.id)}
                              >
                                Excluir
                              </DropdownItem>
                            </DropdownMenu>
                          </UncontrolledDropdown>
                        </td>
                      </tr>
                  )}
                  </tbody>
                </Table>
                <CardFooter className="py-4">
                  <nav aria-label="...">
                    <Paginations
                        fetchAll={props.fetchAll}
                        per_page={props.establishments.pagination.per_page}
                        page={props.establishments.pagination.page}
                        total={props.establishments.pagination.total}
                    />
                  </nav>
                </CardFooter>
              </Card>
            </div>
          </Row>
        </Container>
      </>
  );
}

const mapStateToProps = (state) => ({
  establishments: state.establishmentsReducer,
})

const mapDispatchToProps = (dispatch) => ({
  fetchAll: (search, callback) => dispatch(fetchAll(search, callback)),
  remove: (id, callback) => dispatch(remove(id, callback))
})

export default connect(mapStateToProps, mapDispatchToProps)(Index);

