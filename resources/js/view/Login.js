import React from "react";
import { connect } from 'react-redux';
import { login } from '../store/actions/auth.action';

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Row,
  Col
} from "reactstrap";

import {
  TextField,
} from "@material-ui/core";

import Loading from "../components/Loading/Loading";
import Notify from "../components/Notify/Notify";
import useForm from "../components/useForm";
import {rootUrlAdmin} from "../config/globalConfig";

const initialFieldValues = {
  email: '',
  password: ''
}

const Login = (props) => {

  const validate = () => {
    let temp = {...errors}

    let pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    temp.email = values.email?"":"This email field is required";
    if (!pattern.test(values.email)) {
      temp.email = "This email invalid";
    }
    temp.password = values.password?"":"This password field is required";
    setErrors({
      ...temp
    })
    return Object.values(temp).every(x => x=="");
  }

  let {
    values,
    setValues,
    errors,
    setErrors,
    handleInputChange
  } = useForm(initialFieldValues)

  const onSuccess = () => {
    window.location.replace(rootUrlAdmin + 'admin');
  }

  const onError = (errors) => {
    if(typeof errors != "undefined") {
      setErrors({
        ...errors
      })
    }
  }

  const handleSubmit = e => {
    e.preventDefault();
    if(validate()) {
      props.login(values, function(success, errors){
        if(success) {
          onSuccess();
        } else {
          onError(errors)
        }
      })
    }
  }

  return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardHeader className="bg-transparent pb-5 text-center">
              <img src={require("../assets/img/brand/bingox.png")} />
            </CardHeader>
            <CardBody className="px-lg-5 py-lg-5">
              <Loading/>
              <Notify/>
              <form autoComplete="off" noValidate onSubmit={handleSubmit}>

                <FormGroup className="mb-3">
                  <label
                      className="form-control-label"
                      htmlFor="email"
                  >
                    E-mail
                  </label>
                  <TextField
                      name="email"
                      type="email"
                      variant="outlined"
                      fullWidth
                      value={values.email}
                      onChange={handleInputChange}
                      {...(errors.email && {error:true,helperText:errors.email})}
                  />
                </FormGroup>

                <FormGroup className="mb-3">
                  <label
                      className="form-control-label"
                      htmlFor="password"
                  >
                    Senha
                  </label>
                  <TextField
                      name="password"
                      type="password"
                      variant="outlined"
                      fullWidth
                      value={values.password}
                      onChange={handleInputChange}
                      {...(errors.password && {error:true,helperText:errors.password})}
                  />
                </FormGroup>
                <div className="text-center">
                  <Button
                      className="my-4"
                      color="primary"
                      type="submit">
                    Entrar
                  </Button>
                </div>
              </form>
            </CardBody>
          </Card>
          <Row className="mt-3">
            <Col xs="12">
              <a
                  className="text-light"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
              >
                <small>Esqueceu sua senha?</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
  );
}



const mapStateToProps = (state) => ({
  auth: state.authReducer
})

const mapDispatchToProps = dispatch => ({
  login: (value, callback) => dispatch(login(value, callback))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);
