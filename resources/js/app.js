import React from "react";
import { Provider } from "react-redux";
import ReactDOM from "react-dom";
import { store } from './store/store';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import AdminLayout from "./layouts/Admin";
import AuthLayout from "./layouts/Auth";

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route path="/admin" render={props => <AdminLayout {...props} />} />
                <Route path="/auth" render={props => <AuthLayout {...props} />} />
                <Redirect from="/" to="/auth/login" />
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById("app")
);
