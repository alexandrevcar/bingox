import React, {useState} from "react";

const useForm = (initialFieldValues) => {
    const [values, setValues] = useState(initialFieldValues)
    const [errors, setErrors] = useState([])
    const [user, setUser] = useState({
        label: '',
        value: ''
    })
    const [room, setRoom] = useState({
        label: '',
        value: ''
    })

    const handleInputChange = e => {
        if(typeof e.target != "undefined") {
            const {name, value} = e.target;
            setValues({
                ...values,
                [name]: value
            })
        } else {

            switch (e.type) {
                case 'user':
                    setValues({
                        ...values,
                        user_id: e.value
                    })
                    setUser({
                        value:e.value,
                        label:e.label
                    })
                    console.log(e, 'han');
                    break;
                case 'room':
                    setValues({
                        ...values,
                        room_id: e.value
                    })
                    setRoom({
                        value:e.value,
                        label:e.label
                    })
                    console.log(e, 'han');
                    break;

            }

        }

    }
    return {
        values,
        setValues,
        errors,
        setErrors,
        user,
        setUser,
        room,
        setRoom,
        handleInputChange,

    }
}

export default useForm;