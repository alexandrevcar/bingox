import React  from 'react';
import { connect } from 'react-redux';
import { changeNotify } from '../../store/actions/notify.action';
import Snackbar from '@material-ui/core/Snackbar';
import { UncontrolledAlert } from "reactstrap";

const Notify = (props) => {

    const closeNotify = () => {
        props.changeNotify({
            open: false,
            message: ''
        });
    };

    return (
        <div>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={props.notify.open}
                autoHideDuration={props.notify.time}
                onClose={() => closeNotify()}
            >
                <UncontrolledAlert color={(props.notify.error == false ? "success" : "danger")} fade={false}>
                        <span className="alert-inner--icon">
                            <i className="ni ni-check-bold" />
                        </span>{" "}
                    <span className="alert-inner--text">
                            <strong>{props.notify.message}</strong>
                        </span>
                </UncontrolledAlert>
            </Snackbar>
        </div>
    )

}

const mapStateToProps = (state) => ({
    notify: state.notifyReducer,
})

const mapDispatchToProps = dispatch => ({
    changeNotify: (value) => dispatch(changeNotify(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(Notify);