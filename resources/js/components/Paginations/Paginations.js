import React, {useEffect} from "react";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

const Paginations = (props) => {

    let rows = [];
    let page = props.page;
    let perPage = props.per_page;
    let total = props.total;
    let limitPages = Math.ceil((total/perPage));

    let limit = page+1 > limitPages ? page : limitPages;
    let init = (page-1 < 0 ? 1 : (limitPages-(page-2) >= 2 ? page-2 : page-1));

    for(let i=init; i<=limit; i++) {
        if(i > 0 && i <= limitPages) {
            let activeClass = (i == page) ? 'active' : '';
            rows.push(
                <PaginationItem key={i} className={activeClass}>
                    <PaginationLink onClick={() => openPage(i)}>{i}</PaginationLink>
                </PaginationItem>
            );
        }
    }

    useEffect(() => {
        props.fetchAll({
            page: 1,
            per_page: 3
        });
    }, []);

    const openPage = (page) => {
        props.fetchAll({
            page: page,
            per_page: props.per_page
        });
    }

    return (
        <>
            {props.total &&
            <Pagination
                className="pagination justify-content-end mb-0"
                listClassName="justify-content-end mb-0"
            >
                {props.page > 1 &&
                <PaginationItem>
                    <PaginationLink
                        onClick={() => openPage(page > 1 ? page-1 : 1)}
                        tabIndex="-1"
                    >
                        <i className="fas fa-angle-left"/>
                        <span className="sr-only">Previous</span>
                    </PaginationLink>
                </PaginationItem>
                }
                {rows}
                {props.page+1 <= limitPages &&
                <PaginationItem>
                    <PaginationLink
                        onClick={() => openPage(page < limit ? page+1 : limit)}
                    >
                        <i className="fas fa-angle-right"/>
                        <span className="sr-only">Next</span>
                    </PaginationLink>
                </PaginationItem>
                }
            </Pagination>
            }
        </>
    );

}

export default Paginations;