import React from "react";
// reactstrap components
import {
    Modal,
    Card,
    CardBody,
    CardText,
    Spinner
} from "reactstrap";
import { connect } from 'react-redux';

const Loading = (props) => {
    return (
        <>
            {/* Modal */}
            <Modal
                className="modal-dialog-centered"
                isOpen={props.loading.open}
            >
                <Card>
                    <CardBody>
                        <CardText className="text-center">
                            <Spinner size="sm" color="secundary" />
                            <span className="alert-inner--text">
                                    <strong> {props.loading.msg}</strong>
                                </span>
                        </CardText>
                    </CardBody>
                </Card>
            </Modal>
        </>
    );

}

const mapStateToProps = (state) => ({
    loading: state.loadingReducer
})

export default connect(mapStateToProps)(Loading);