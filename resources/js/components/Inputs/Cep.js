import React from "react";
import MaskedInput from 'react-text-mask';

const Cep = (props) => {
    const { inputRef, ...other } = props;

    return (
        <MaskedInput
            {...other}
            ref={ref => {inputRef(ref ? ref.inputElement : null); }}
            mask={[/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}
            placeholderChar={'\u2000'}
            guide="false"
        />
    );
}

export default Cep;